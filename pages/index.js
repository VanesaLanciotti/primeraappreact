function Figura1() {
    return (
        <img src="./static/figura1.jpg" alt="" width="300"/> //por default para usar imagenes hay que crear la carpeta llamada static en el proyecto.
     )
}

function Seccion1() {
    return (
        <seccion>
            <h2>Esta no es la seccion 1</h2>
            <Figura1/>
        </seccion>
    )

}
function Index() {
    return (
        <div>
            <h1>Bienvenido  a mi primera app con REACT</h1>
            <p>Esta es mi primera aplicacion con next.js</p>
            <Seccion1/>
        </div>

    )
}
export default Index
